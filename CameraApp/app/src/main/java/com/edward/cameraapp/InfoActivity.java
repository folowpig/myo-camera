package com.edward.cameraapp;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceScreen;
import android.view.View;

/**
 * Created by Ed on 3/20/2016.
 */
public class InfoActivity extends PreferenceFragment {
    private String AppVersion = "unknown";
    private MainActivity mMainActivity;

    public static Fragment newInstance() {
        return new InfoActivity();
    }

    @Override
    public void onAttach(Activity activity) {
        mMainActivity = (MainActivity) activity;
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.info);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        view.setFitsSystemWindows(true);
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        try {
            AppVersion = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e1) {
            e1.printStackTrace();
        }
        Preference appVersion = findPreference("app.version");
        if (appVersion != null) {
            appVersion.setSummary(AppVersion);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }



    @Override
    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
        if (preference != null && preference.getKey() != null) {
            if (preference.getKey().equals("myo.info")) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.thalmic.com/en/myo/")));
            } else if (preference.getKey().equals("myo.purchase")) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.thalmic.com/en/myo/preorder/")));
            } else if (preference.getKey().equals("help.mail")) {
                createSupportEmail();
            } else if (preference.getKey().equals("source.bitbucket")) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://bitbucket.org/folowpig/myo-camera")));
            } else if (preference.getKey().equals("app.version")) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=eu.thedarken.myo.twothousandfortyeight")));
            }

        }
        return super.onPreferenceTreeClick(preferenceScreen, preference);
    }

    private void createSupportEmail() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("message/rfc822");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"edkim90@icloud.com"});

        String subject = "[MYO Camera] Questions/Concerns/Feedback";
    }
}
