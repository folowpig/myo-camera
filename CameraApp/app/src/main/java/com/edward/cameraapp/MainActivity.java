package com.edward.cameraapp;


import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.ShutterCallback;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.Toast;

import com.thalmic.myo.AbstractDeviceListener;
import com.thalmic.myo.Arm;
import com.thalmic.myo.DeviceListener;
import com.thalmic.myo.Hub;
import com.thalmic.myo.Myo;
import com.thalmic.myo.Pose;
import com.thalmic.myo.Quaternion;
import com.thalmic.myo.XDirection;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class MainActivity extends Activity {
    private static final String TAG = "MainAcitvity";
    private static String tag = "Myo Armband"; // For Toast message
    private Hub mHub;
    public static boolean Reversed = false;
    private Toast Toast;  // Toast message
    cameraPreview cameraPreview;
    Camera camera;
    Activity act;
    Context context;
    ImageButton cMyoButton;
    ImageButton cInfoButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        act = this;

        /*Set camera application with no title bar and full screen */
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);
        cMyoButton = (ImageButton)findViewById(R.id.mini_myo);
        cInfoButton = (ImageButton)findViewById(R.id.info);
        cameraPreview = new cameraPreview(this, (SurfaceView)findViewById(R.id.surfaceView));
        cameraPreview.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        ((FrameLayout) findViewById(R.id.layout)).addView(cameraPreview);
        cameraPreview.setKeepScreenOn(true);

        /*
        mHub = Hub.getInstance();
        if (!mHub.init(this)) {
            Log.e(TAG, "Could not initialize the Hub.");
            finish();
            return;
        }
        mHub.addListener(mListener);
        mHub.setLockingPolicy(Hub.LockingPolicy.NONE);

        /*
        cameraPreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                camera.takePicture(shutterCallback, rawCallback, jpegCallback);
            }
        });
        */
        cMyoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mHub != null) {
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    Fragment fragment = getFragmentManager().findFragmentByTag(InfoActivity.class.getName());
                    if (fragment == null)
                        fragment = TutorialFragment.newInstance();
                    transaction.setCustomAnimations(R.anim.fragment_anim_normal_enter, R.anim.fragment_anim_normal_exit, R.anim.fragment_anim_normal_enter, R.anim.fragment_anim_normal_exit);
                    transaction.replace(R.id.layout, fragment, TutorialFragment.class.getName());
                    transaction.addToBackStack(MainActivity.class.getName());
                    transaction.commit();
                } else {
                    displayToast("Could not find Myo");
                }
            }
        });

        cInfoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                Fragment fragment = getFragmentManager().findFragmentByTag(InfoActivity.class.getName());
                if (fragment == null)
                    fragment = InfoActivity.newInstance();
                transaction.setCustomAnimations(R.anim.fragment_anim_normal_enter, R.anim.fragment_anim_normal_exit, R.anim.fragment_anim_normal_enter, R.anim.fragment_anim_normal_exit);
                transaction.replace(R.id.layout, fragment, InfoActivity.class.getName());
                transaction.addToBackStack(MainActivity.class.getName());
                transaction.commit();
            }
        });

        displayToast(getString(R.string.take_photo_help));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }



    @Override
    protected void onResume() {
        super.onResume();
        int numCams = Camera.getNumberOfCameras();
        if(numCams > 0){
            try{
                camera = Camera.open(0);
                camera.startPreview();
                cameraPreview.setCamera(camera);
            } catch (RuntimeException ex){
                displayToast(getString(R.string.camera_not_found));
            }
        }
    }

    @Override
    protected void onPause() {
        if(camera != null) {
            camera.stopPreview();
            cameraPreview.setCamera(null);
            camera.release();
            camera = null;
        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // We don't want any callbacks when the Activity is gone, so unregister the listener.
        Hub.getInstance().removeListener(mListener);
        if (isFinishing()) {
            // The Activity is finishing, so shutdown the Hub. This will disconnect from the Myo.
            Hub.getInstance().shutdown();
        }
    }

    public Hub getmHub() {
        if (mHub == null) {
            mHub = Hub.getInstance();
            if(!mHub.init(this)) {
                Logy.e(TAG, "Could not initialize the Hub.");
                mHub = null;
            }
        }
        return mHub;
    }

    private void resetCam() {
        camera.startPreview();
        cameraPreview.setCamera(camera);
    }

    private void refreshGallery(File file) {
        Intent mediaScanIntent = new Intent( Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        mediaScanIntent.setData(Uri.fromFile(file));
        sendBroadcast(mediaScanIntent);
    }

    ShutterCallback shutterCallback = new ShutterCallback() {
        public void onShutter() {
        }
    };

    PictureCallback rawCallback = new PictureCallback() {
        public void onPictureTaken(byte[] data, Camera camera) {
        }
    };

    PictureCallback jpegCallback = new PictureCallback() {
        public void onPictureTaken(byte[] data, Camera camera) {
            new SaveImageTask().execute(data);
            resetCam();
            Log.d(TAG, "onPictureTaken - jpeg");
        }
    };

    private class SaveImageTask extends AsyncTask<byte[], Void, Void> {

        @Override
        protected Void doInBackground(byte[]... data) {
            FileOutputStream outputStream = null;

            // Write to SD Card or if SD card is not available save on device with directory DCIM/MYOCAMERA
            try {
                File sdCard = Environment.getExternalStorageDirectory();
                File dir = new File (sdCard.getAbsolutePath() + "DCIM/MYOCAMERA");
                dir.mkdirs();
                // Save file with current time in millisecond
                String fileName = String.format("%d.jpg", System.currentTimeMillis());
                File outFile = new File(dir, fileName);

                outputStream = new FileOutputStream(outFile);
                outputStream.write(data[0]);
                outputStream.flush();
                outputStream.close();

                Log.d(TAG, "onPictureTaken - wrote bytes: " + data.length + " to " + outFile.getAbsolutePath());

                refreshGallery(outFile);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
            }
            return null;
        }
    }

    // Displays Toast message to screen
    public void displayToast(String s) {
        Log.w(tag, s);
        if (Toast == null)
            Toast = Toast.makeText(this, s, Toast.LENGTH_SHORT);
        else
            Toast.setText(s);
        Toast.show();
    }

    private DeviceListener mListener = new AbstractDeviceListener() {
        private Arm rArm = Arm.UNKNOWN;
        private XDirection direction = XDirection.UNKNOWN;

        // Check Myo Armband is attached to arm
        @Override
        public void onAttach(Myo myo, long timestamp) {
            displayToast("Myo is attached");
        }

        @Override
        public void onDetach(Myo myo, long timestamp) {
            displayToast("Myo is detached from arm");
        }

        // Check Myo is connected and gives short vibration
        @Override
        public void onConnect(Myo myo, long timestamp) {
            displayToast("Myo is connected");
            myo.vibrate(Myo.VibrationType.SHORT);
        }
        // If it is disconnected then gives medium length vibration
        @Override
        public void onDisconnect(Myo myo, long timestamp) {
            displayToast("Myo is disconnected");
            myo.vibrate(Myo.VibrationType.MEDIUM);
        }

        @Override
        public void onArmSync(Myo myo, long timestamp, Arm arm, XDirection xDirection) {
            rArm = arm;
            direction = xDirection;
            displayToast("Myo is synced to " + rArm + " direction: " + direction);
        }

        @Override
        public void onArmUnsync(Myo myo, long timestamp) {
            rArm = Arm.UNKNOWN;
            direction = XDirection.TOWARD_ELBOW.UNKNOWN;
            displayToast("Myo lost sync");
        }

        @Override
        public void onUnlock(Myo myo, long timestamp) {
            displayToast("Myo is unlocked for use");
        }

        @Override
        public void onLock(Myo myo, long timestamp) {
            displayToast("Myo is locked perform unlocking gesture");
        }

        @Override
        public void onPose(Myo myo, long timestamp, Pose pose) {
            switch (pose) {
                case UNKNOWN:
                    break;
                case REST:
                    displayToast("Myo is resting it will be locked soon");
                    break;
                case FINGERS_SPREAD:
                    displayToast("Finger Spread: Picture is taken");
                    try {
                        camera.takePicture(shutterCallback, rawCallback, jpegCallback);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    myo.vibrate(Myo.VibrationType.SHORT);   //Gives two little vibration if photo is taken.
                    myo.vibrate(Myo.VibrationType.SHORT);
                    break;
                case DOUBLE_TAP:
                    displayToast("Double tap: Autofocus");
                    camera.getParameters().setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
                    break;
                case WAVE_IN:
                    displayToast("Wave in: Flash On");
                    camera.getParameters().setFlashMode(Camera.Parameters.FLASH_MODE_ON);
                case WAVE_OUT:
                    displayToast("Wave out: Flash Off");
                    camera.getParameters().setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                    break;
                case FIST:
                    displayToast("Fist: ");
                    break;
            }
            // Tell Myo Armband to stay unlocked if it is moving
            if (pose != Pose.UNKNOWN || pose != Pose.REST) {
                myo.unlock(Myo.UnlockType.HOLD);
            }
            else
                myo.unlock(Myo.UnlockType.TIMED);
        }

        @Override
        public void onOrientationData(Myo myo, long timestamp, Quaternion rotation) {
            float roll = (float) Math.toDegrees(Quaternion.roll(rotation));
            float pitch = (float) Math.toDegrees(Quaternion.pitch(rotation));
            float yaw = (float) Math.toDegrees(Quaternion.yaw(rotation));

            if (direction == XDirection.TOWARD_ELBOW) {
                roll *= -1;
                pitch *= -1;
            }
        }
    };
}



