package com.edward.cameraapp;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thalmic.myo.Arm;
import com.thalmic.myo.DeviceListener;
import com.thalmic.myo.Hub;
import com.thalmic.myo.Myo;
import com.thalmic.myo.Pose;
import com.thalmic.myo.Quaternion;
import com.thalmic.myo.Vector3;
import com.thalmic.myo.XDirection;
import com.thalmic.myo.scanner.ScanActivity;

/**
 * Created by Ed on 3/25/2016.
 */


public class TutorialFragment extends Fragment implements DeviceListener {
    private static final String TAG = "TutorialFragment";
    private TextView myoConnect, myoSetup, whenReady, takeNow;
    private ImageView leftGesture, rightGesture, downGesture, upGesture;
    private LinearLayout mControlLayout, upBox, downBox, leftBox, rightBox;
    private Hub mMyoHub;
    private static final long REFRESHINTERVAL = 250;
    private boolean mConnected = false;
    private boolean mSynced = false;
    private Animation mAnimWiggle;

    public static Fragment newInstance() {
        return new TutorialFragment();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mMyoHub = ((MainActivity) activity).getmHub();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.tutorial_layout, container, false);
        myoConnect = (TextView) layout.findViewById(R.id.tv_connect_myo);
        myoSetup = (TextView) layout.findViewById(R.id.tv_setup_myo);
        whenReady = (TextView) layout.findViewById(R.id.tv_when_ready);
        takeNow = (TextView) layout.findViewById(R.id.tv_play_now);
        mControlLayout = (LinearLayout) layout.findViewById(R.id.ll_controls);
        leftGesture = (ImageView) layout.findViewById(R.id.iv_gesture_left);
        rightGesture = (ImageView) layout.findViewById(R.id.iv_gesture_right);
        upGesture = (ImageView) layout.findViewById(R.id.iv_gesture_up);
        downGesture = (ImageView) layout.findViewById(R.id.iv_gesture_down);

        upBox = (LinearLayout) layout.findViewById(R.id.ll_up_layout);
        downBox = (LinearLayout) layout.findViewById(R.id.ll_down_layout);
        leftBox = (LinearLayout) layout.findViewById(R.id.ll_left_layout);
        rightBox = (LinearLayout) layout.findViewById(R.id.ll_right_layout);
        return layout;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        myoConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mMyoHub != null) {
                    Intent intent = new Intent(getActivity(), ScanActivity.class);
                    startActivity(intent);
                }
            }
        });
        myoSetup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mMyoHub.getConnectedDevices().isEmpty()) {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse("https://support.getmyo.com/hc/en-us/articles/200755509-How-to-perform-the-sync-gesture"));
                    startActivity(intent);
                }
            }
        });
        takeNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });
        refresh();
        if (getView() != null)
            getView().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (getView() != null && isAdded()) {
                        getView().postDelayed(this, REFRESHINTERVAL);
                        refresh();
                    }
                }
            }, REFRESHINTERVAL);

        if (mMyoHub != null) {
            mMyoHub.addListener(this);
        }

        mAnimWiggle = AnimationUtils.loadAnimation(getActivity(), R.anim.wiggle);
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        if (mMyoHub != null)
            mMyoHub.removeListener(this);
        super.onDestroy();
    }

    private void setArm(boolean left) {
        if (left) {
            MainActivity.Reversed = true;
            leftGesture.setImageResource(R.drawable.ic_pose_waveleft_lh);
            rightGesture.setImageResource(R.drawable.ic_pose_waveright_lh);
            upGesture.setImageResource(R.drawable.ic_pose_spread_lh);
            downGesture.setImageResource(R.drawable.ic_pose_fist_lh);
        } else {
            MainActivity.Reversed = false;
            leftGesture.setImageResource(R.drawable.ic_pose_waveleft);
            rightGesture.setImageResource(R.drawable.ic_pose_waveright);
            upGesture.setImageResource(R.drawable.ic_pose_spread);
            downGesture.setImageResource(R.drawable.ic_pose_fist);
        }
    }

    private void refresh() {
        if (mMyoHub != null) {
            setArm(MainActivity.Reversed);
            myoConnect.setVisibility(mConnected ? View.GONE : View.VISIBLE);
            myoSetup.setVisibility(mConnected && !mSynced ? View.VISIBLE : View.GONE);
            whenReady.setVisibility(mConnected && mSynced ? View.VISIBLE : View.GONE);
            takeNow.setVisibility(mConnected && mSynced ? View.VISIBLE : View.GONE);
            mControlLayout.setVisibility(mConnected && mSynced ? View.VISIBLE : View.GONE);
        } else {
            myoConnect.setVisibility(View.GONE);
            myoSetup.setTextColor(View.GONE);
            myoSetup.setVisibility(View.GONE);
            whenReady.setVisibility(View.GONE);
            takeNow.setVisibility(View.GONE);
            mControlLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void onAttach(Myo myo, long l) {
        Logy.d(TAG, "onAttach");
    }

    @Override
    public void onDetach(Myo myo, long l) {
        Logy.d(TAG, "onDetach");
    }

    @Override
    public void onConnect(Myo myo, long l) {
        Logy.d(TAG, "onConnect");
        mConnected = true;
        if (myo.isUnlocked())
            mSynced = true;
    }

    @Override
    public void onDisconnect(Myo myo, long l) {
        Logy.d(TAG, "onDisconnect");
        mConnected = false;
    }

    @Override
    public void onArmSync(Myo myo, long l, Arm arm, XDirection xDirection) {
        Logy.d(TAG, "onArmSync");
        mSynced = true;
        MainActivity.Reversed = arm == Arm.LEFT;
        myo.unlock(Myo.UnlockType.HOLD);
    }

    @Override
    public void onArmUnsync(Myo myo, long l) {
        Logy.d(TAG, "onArmUnsync");
        mSynced = false;
    }

    @Override
    public void onUnlock(Myo myo, long l) {
        Logy.d(TAG, "onUnlock");
    }

    @Override
    public void onLock(Myo myo, long l) {
        Logy.d(TAG, "onLock");
    }

    @Override
    public void onPose(Myo myo, long l, Pose pose) {
        Logy.i(TAG, "onPose:" + pose.name());
        if (!isResumed())
            return;

        if (pose == Pose.FIST) {
            downBox.startAnimation(mAnimWiggle);
        } else if (pose == Pose.WAVE_OUT) {
            if (MainActivity.Reversed) {
                leftBox.startAnimation(mAnimWiggle);
            } else {
                rightBox.startAnimation(mAnimWiggle);
            }
        } else if (pose == Pose.WAVE_IN) {
            if (MainActivity.Reversed) {
                rightBox.startAnimation(mAnimWiggle);
            } else {
                leftBox.startAnimation(mAnimWiggle);
            }
        } else if (pose == Pose.FINGERS_SPREAD) {
            upBox.startAnimation(mAnimWiggle);
        }
    }

    @Override
    public void onOrientationData(Myo myo, long l, Quaternion quaternion) {

    }

    @Override
    public void onAccelerometerData(Myo myo, long l, Vector3 vector3) {

    }

    @Override
    public void onGyroscopeData(Myo myo, long l, Vector3 vector3) {

    }

    @Override
    public void onRssi(Myo myo, long l, int i) {

    }
}